#!/usr/bin/env bash

echo -e "## Linting results" > /results/xargs.txt
find /code_to_analyse/src -name "*.php" | xargs -n1 php -l > /results/xargs.txt
find /code_to_analyse/public -name "*.php" | xargs -n1 php -l > /results/xargs.txt
echo "xargs executed"

phploc  --count-tests /code_to_analyse/ > /results/phploc.txt 2>&1
echo "phploc executed"

phpcpd /code_to_analyse/src /code_to_analyse/public /code_to_analyse/tests > /results/cpd.txt 2>&1
echo "php copy paste detector executed"

phan -d /code_to_analyse/ --output-mode json > /results/phan.json
echo "phan executed"

phpcs /code_to_analyse/ > /results/phpcs.txt
echo "php code sniffer executed"


phpstan analyse \
    --autoload-file=/code_to_analyse/vendor/autoload.php \
    /code_to_analyse/src /code_to_analyse/public \
    --level=7 \
    > /results/phpstan.txt
echo "phpstan executed"

phpmetrics --report-html=/metrics /code_to_analyse/
chmod -R 775 /metrics
echo "phpmetrics site generated"

phpdoc -d /code_to_analyse/src,/code_to_analyse/public -t /docs \
    --defaultpackagename Laudis\Calculators \
    --title package \
    --template="clean" \
    &> /dev/null
echo "phpdoc site generated"


