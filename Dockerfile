FROM php:7.3-cli

COPY .docker/composer-installer.sh /usr/local/bin/composer-installer

COPY .docker/static-analysis/php/php.ini $PHP_INI_DIR/php.ini


RUN apt-get update \
    && apt-get install -yqq --no-install-recommends libzip-dev zip unzip curl libicu-dev libbz2-dev graphviz wget bash \
    && docker-php-ext-configure intl \
    && docker-php-ext-install pdo_mysql opcache zip bz2 intl pcntl bcmath \
    && cp $PHP_INI_DIR/php.ini-production $PHP_INI_DIR/php.ini \
    && pecl config-set php_ini $PHP_INI_DIR/php.ini \
    && pecl install ast \
    && pecl install redis-4.2.0 \
    && docker-php-ext-enable redis \
    && echo "extension=ast.so" >> $PHP_INI_DIR/php.ini \
    && chmod +x /usr/local/bin/composer-installer \
    && composer-installer \
    && mv composer.phar /usr/local/bin/composer \
    && wget http://www.phpdoc.org/phpDocumentor.phar \
    && chmod +x phpDocumentor.phar \
    && mv phpDocumentor.phar /usr/local/bin/phpdoc \
    && chmod +x /usr/local/bin/ \
    && mkdir /opt/composer \
    && COMPOSER_HOME=/opt/composer COMPOSER_BIN_DIR=/usr/local/bin COMPOSER_VENDOR_DIR=/opt/composer/vendor composer global require hirak/prestissimo --no-plugins --no-scripts \
    && COMPOSER_HOME=/opt/composer COMPOSER_BIN_DIR=/usr/local/bin COMPOSER_VENDOR_DIR=/opt/composer/vendor composer global require \
        phploc/phploc \
        phan/phan \
        phpstan/phpstan \
        sebastian/phpcpd \
        squizlabs/php_codesniffer \
        phpmetrics/phpmetrics \
        --no-interaction \
        --prefer-dist \
        --no-progress \
        --no-scripts \
        --profile \
    && unlink /etc/localtime \
    && ln -s /usr/share/zoneinfo/Europe/Brussels /etc/localtime

COPY .docker/composer-installer.sh .
RUN composer-installer \
    && mv composer.phar /usr/local/bin/composer \
    && chmod +x /usr/local/bin/composer

WORKDIR /
ADD .docker/static-analysis/php/run.sh /
RUN chmod +x run.sh \
    && mkdir code_to_analyse \
    && composer global require hirak/prestissimo --no-plugins --no-scripts \
    && mkdir caching


WORKDIR /caching
COPY composer.json composer.loc[k] ./
RUN composer install \
        --no-interaction \
        --prefer-dist \
        --no-progress \
        --no-scripts \
        --profile \
        --no-autoloader \
        --ignore-platform-reqs


WORKDIR /code_to_analyse

ADD composer.json composer.loc[k] ./
ADD src/ src/
ADD public/ public/
ADD tests/ tests/
ADD .docker/static-analysis/php/.phan .phan

RUN composer install \
        --no-interaction \
        --prefer-dist \
        --no-progress \
        --no-scripts \
        --profile \
        --ignore-platform-reqs \
    && composer dumpautoload

WORKDIR /
CMD ["./run.sh"]

